# React Project

A clean react project to start development of your idea.

# Tutorials

Follow a tutorial for setting up a todo app in react [here](https://www.boorje.com/react-todo-app/).

List of project for beginners at [reactbeginners](https://reactbeginnerprojects.com/).

### Setup

 - Install [Node](https://nodejs.org/en/download/), preferably on linux/mac or wsl. Or start git bash if you are using windows without wsl.
 - Clone this repo.
 - Navigate to the directory you cloned in your terminal.
 - Then run  `npm install`.
 - Then run `npm start` to compile your js and css.
 - Add npm packages as needed to the project.

# Support

Contact devs at Zpirit for help.

Øyvind Eikeland oyvind@zpirit.no

Roar Helland roar@zpirit.no

Espen Kvalheim espen@zpirit.no
